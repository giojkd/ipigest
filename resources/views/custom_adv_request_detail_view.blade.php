<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your html goes here -->
<div class='panel panel-default'>
  <div class='panel-heading'>Edit Form</div>
  <div class='panel-body'>
    <h2>Richiesta ADV</h2>
    <table class="table table-hover table-striped">
      <tbody>
        <?php
        foreach ($request as $key => $value):
          if(!in_array($key,$excluded_fields)){
            $label = '';?>
            <tr>
              <td><b><?php foreach($form as $formVal){if($formVal['name']==$key) $label = $formVal['label'];} if($label !='' ) echo $label; else echo $key?></b></td>
              <td><?=$value?></td>
            </tr>
            <?php
          }
        endforeach; ?>
        <tr>
          <td><b>Utente</b></td>
          <td><?=$user['name']?></td>
        </tr>
        <tr>
          <td><b>Agenzia</b></td>
          <td><?=$agency['name']?></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<?php
if($currentStep < 4){
  foreach($feedbacksByStep as $step => $feedbacks){?>
    <div class="panel panel-default">
      <div class="panel-heading" style="background-color:<?=$step_colors[$step]?>">
        <h3 class="panel-title" style="color:#fff"><?=$step_names[$step]?></h3>
      </div>

      <div class="panel-body">
        <div class="row">
          <?php

          $feedback_buttons = $buttonsByStep[$step];

          if(count($feedback_buttons)>0){?>
            <div class="col-md-4">
              <form class="" action="<?php echo CRUDBooster::adminPath(); ?>/adv_request_feedbacks/add-save" method="post">
                <input type="hidden" name="_token" value="<?=csrf_token();?>">
                <input type="hidden" name="cms_user_id" value="<?=CRUDBooster::myId()?>">
                <input type="hidden" name="adv_request_id" value="<?=$request['id']?>">
                <input type="hidden" name="step" value="<?=$step?>">
                <div class="form-group">
                  <label for="">Notes</label>
                  <textarea name="feedback_notes" rows="8" cols="80" class="form-control"></textarea>
                  <p class="help-block">Spiega il motivo della tua scelta</p>
                </div>
                <?php foreach ($feedback_buttons as $key => $button): ?>
                  <button class="btn btn-<?=$button['type']?>" type="submit" value="<?=$button['value']?>" name="feedback_type"><?=$button['label']?></button>
                <?php endforeach; ?>
              </form>

            </div>
          <?php }?>

          <div class="col-md-<?=(count($feedback_buttons)>0) ? '8' : '12'?>">
            <?php if($currentStep == 1){?>
              <a style="margin-top: 10px;" href="/request-approval/<?=$request['id']?>" class="btn btn-default">Richiedi approvazione</a>
            <?php }?>
            <?php if(count($feedbacks) == 0){?>
              <div class="alert alert-info">Non è stato rilasciato ancora nessun feedback per la richiesta</div>
            <?php }else{

              ?>
              <div class="" style="max-height: 320px; overflow-y: auto">


                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Utente</th>
                      <th>Feedback</th>
                      <th>Note</th>
                      <th>Data</th>
                    </tr>
                  </thead>
                  <tbody>


                    <?php
                    foreach($feedbacks as $feedback){

                      ?>
                      <tr>
                        <td>
                          <?=$feedback['user']['name'] ?>
                          <?php
                          if(count($feedback['user']['groups'])){
                            foreach($feedback['user']['groups'] as $group){
                              ?>
                              <span class="label label-default"><?=$group['label']?></span>
                              <?php
                            }
                          } ?>
                        </td>
                        <td> <span class="label label-<?=$feedback_labels[$feedback['feedback_type']]?>"><?=$feedback['feedback_type']?></span></td>
                        <td><?=$feedback['feedback_notes']?></td>
                        <td><?=date('d/m/Y H:i',strtotime($feedback['created_at']))?></td>

                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <?php
            }?>
          </div>
        </div>
      </div>
    </div>
  <?php }} else{
    ?>
    <div class="alert alert-success">
      La richiesta ha completato il percorso di approvazione
    </div>
    <?php
  }?>
  @endsection
