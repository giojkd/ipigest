<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  #return view('welcome');
  #phpinfo();
});

Route::get('/test-email','LeadController@sendTestEmail');

Route::get('/request-approval/{id}','AdminAdvRequestsController@requestApproval');


/*
Route::any('/facebook-webhook',function(){
 $challenge = $_REQUEST['hub_challenge'];
  $verify_token = $_REQUEST['hub_verify_token'];

  if ($verify_token === 'abc123') {
    echo $challenge;
  }

});
*/
