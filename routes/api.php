<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
  return $request->user();
})->middleware('auth:api');

Route::any('/facebook-webhook','LeadController@FacebookWebhook');
Route::get('/parse-facebook-leads','LeadController@ParseFacebookLeads');

/*
# probios #

Route::any('/probios/categories',function(){
  $array = [
    [
      'id' => 'id in as400 (int,11)',
      'name' => 'category name (string,255)',
      'parent_id' => 'id of the parent category in as400 if any (int,11,nullable)',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 'id in as400 (int,11)',
      'name' => 'category name (string,255)',
      'parent_id' => 'id of the parent category in as400 if any (int,11,nullable)',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});


Route::any('/probios/products',function(){
  $array = [
    [
      'id' => 'id in as400 (int,11)',
      'sku' => 'product sku (string,25)',
      'name' => 'product name (string,255)',
      'description' => 'producr description (longtext,nullable)',
      'category_id' => 'id of the product category in as400 (int,11)',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)',
      'brand_id' => 1,
      'may_contain_traces_of' => [
        'list' => [1,2,3,'ids of traces'],
        'text' => 'describes potentially contained traces of other aliments if a list could not be provided'
      ],
      'package' => [
        'quantity' => 'int (nullable)',
        'individual_weight' => 'float (nullable)',
        'total_weight' => 'float (nullable) if left empty will be calculated by quantity * individual_weight',
        'unit_of_measurement' => 'g,kg,ml,cl etc...'
      ],
      'ingredients' => [
        'list' => [
          [
            'id' => 'id of the ingredient',
            'value' => 'ingredient value',
            'unit_of_measurement' => 'g or %'
          ]
        ],
        'text' => 'describes ingredients in case an array of ingredients cannot be provided (longtext,nullable)'
      ],
      'preservation_method' => 'preservation method description (longtext,nullable)',
      'usage_advices' => 'usage advices (longtext,nullable)',
      'highlight_notes' => 'to be noted before buying (longtext,nullable)',
      'stock' => [
        [
          'quantity' => 10,
          'expiration_date' => '(date,YYYYMMDD,nullable)',
          'pieces_in_bundle' => 'how many pieces are there in the bundle (int,11,1 to n)',
          'price' => 'price per bundle (float)',
          'weight' => [
            'value' => 100,
            'unit_of_measurement'=> 'g'
          ],
          'size' => [
            'height' => [
              'value' => '20.5',
              'unit_of_measurement' => 'cm'
            ],
            'width' => [
              'value' => '2.5',
              'unit_of_measurement' => 'cm'
            ],
            'depth' => [
              'value' => '250',
              'unit_of_measurement' => 'allowed units are: cm,mm,inches'
            ]
          ]
        ]
      ],
      'nutritional_value' => [
        [
          'group_quantity' => '100',
          'group_unit_of_measurement' => 'g',
          'values' => [
            [
              'nutritional_value_id' => '1',
              'nutritional_value_value' => '322',
              'nutritionoal_value_unit_of_measurement' => '322'
            ]
          ]
        ]
      ],
      'recyclable_wastes' => [
        [
          'package_component_id' => '1',
          'material_id' => '1'
        ]
      ],
      'binary_values' => [
        '11',
        '34',
        'only enabled ones'
      ],
      'origins_of_raw_materials_id' => 'origins option id',
      'weight' => [
        'value' => '100',
        'unit_of_measurement' => 'g'
      ]
    ]
  ];
  return $array;
});

Route::any('/probios/collections',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Senza Glutine',
      'description' => '',
      'collection_type' => 'group|line',
      'products_list' => [1,2,3,4],
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/materials',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Plastica',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Tetrapak',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/brands',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Idea toscana',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Nutrimento bio',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/package_components',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Tappo',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Sacchetto',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 3,
      'label' => 'Brick',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/origins',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Senza sale aggiunto',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Certificazione Kosher',
      'description' => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/origins',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Agricoltura UE',
      'description'  => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Agricoltura NON UE',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/traces',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Frutta a guscio',
      'description'  => 'vale a dire: mandorle (Amygdalus communis L.), nocciole (Corylus avellana), noci (Juglans regia), noci di acagiù (Anacardium occidentale), noci di pecan [Carya illinoinensis (Wangenh.) K. Koch], noci del Brasile (Bertholletia excelsa), pistacchi (Pistacia vera), noci macadamia o noci del Queensland (Macadamia ternifolia).',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Latte',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});

Route::any('/probios/ingredients',function(){
  $array = [
    [
      'id' => 1,
      'label' => 'Farina di grano',
      'description'  => '',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ],
    [
      'id' => 2,
      'label' => 'Farina di grano tenero tipo 0',
      'updated_at' => 'last update date (date,YYYYMMDD,nullable)'
    ]
  ];
  return $array;
});
*/
