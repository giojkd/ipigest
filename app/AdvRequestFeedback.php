<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvRequestFeedback extends Model
{
    protected $table = 'adv_request_feedbacks';

    public function adv_request(){
      return $this->belongsTo('\App\AdvRequest','adv_request_id','id');
    }

    public function user(){
      return $this->belongsTo('\App\CmsUser','cms_user_id','id');
    }


}
