<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDbooster;

class AdminCmsUsersController extends \crocodicstudio\crudbooster\controllers\CBController {


	public function cbInit() {
		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->table               = 'cms_users';
		$this->primary_key         = 'id';
		$this->title_field         = "name";
		$this->button_action_style = 'button_icon';
		$this->button_import 	   = FALSE;
		$this->button_export 	   = FALSE;
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = array();
		$this->col[] = array("label"=>"ID","name"=>"id");
		$this->col[] = array("label"=>"Name","name"=>"name");
		$this->col[] = array("label"=>"Email","name"=>"email");
		$this->col[] = array("label"=>"Privilege","name"=>"id_cms_privileges","join"=>"cms_privileges,name");
		#$this->col[] = ["label"=>"Total Favorite","name"=>"(select count(favorite.id) from favorite where favorite.products_id = products.id) as total_favorite"];

		$this->col[] = array("label"=>"Gruppi","name"=>"(SELECT GROUP_CONCAT(label) FROM user_groups LEFT JOIN cms_user_user_group ON user_groups.id = cms_user_user_group.user_group_id WHERE cms_user_user_group.cms_user_id = cms_users.id GROUP BY cms_user_user_group.cms_user_id) as user_groups");
		$this->col[] = array("label"=>"Agenzie","name"=>"(SELECT GROUP_CONCAT(name) FROM agencies LEFT JOIN agency_cms_user ON agencies.id = agency_cms_user.agency_id WHERE agency_cms_user.cms_user_id = cms_users.id GROUP BY agency_cms_user.cms_user_id) as agencies");
		#$this->col[] = array("label"=>"Photo","name"=>"photo","image"=>1);
		#$this->col[] = array("label"=>"Phone","name"=>"phone");
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = array();
		$this->form[] = array("label"=>"Name","name"=>"name",'required'=>true,'validation'=>'required|alpha_spaces|min:3');
		$this->form[] = array("label"=>"Email","name"=>"email",'required'=>true,'type'=>'email','validation'=>'required|email|unique:cms_users,email,'.CRUDBooster::getCurrentId());
		$this->form[] = array("label"=>"Photo","name"=>"photo","type"=>"upload","help"=>"Recommended resolution is 200x200px",'validation'=>'image|max:1000','resize_width'=>90,'resize_height'=>90);
		$this->form[] = array("label"=>"Phone","name"=>"phone");
		$this->form[] = array("label"=>"Privilege","name"=>"id_cms_privileges","type"=>"select","datatable"=>"cms_privileges,name",'required'=>true);
		// $this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");
		$this->form[] = array("label"=>"Password","name"=>"password","type"=>"password","help"=>"Please leave empty if not change");
		$this->form[] = array("label"=>"Password Confirmation","name"=>"password_confirmation","type"=>"password","help"=>"Please leave empty if not change");


		$columns[] = ['label'=>'Agency','name'=>'agency_id','type'=>'datamodal','datamodal_table'=>'agencies','datamodal_columns'=>'name','datamodal_size'=>'small'];
		$this->form[] = ['label'=>'Agencies','name'=>'agency_cms_user','type'=>'child','columns'=>$columns,'table'=>'agency_cms_user','foreign_key'=>'cms_user_id'];


		$groups[] = ['label'=>'Groups','name'=>'user_group_id','type'=>'datamodal','datamodal_table'=>'user_groups','datamodal_columns'=>'label','datamodal_size'=>'small'];
		$this->form[] = ['label'=>'Groups','name'=>'cms_user_user_group','type'=>'child','columns'=>$groups,'table'=>'cms_user_user_group','foreign_key'=>'cms_user_id'];
		# END FORM DO NOT REMOVE THIS LINE

	}

	public function getProfile() {

		$this->button_addmore = FALSE;
		$this->button_cancel  = FALSE;
		$this->button_show    = FALSE;
		$this->button_add     = FALSE;
		$this->button_delete  = FALSE;
		$this->hide_form 	  = ['id_cms_privileges'];

		$data['page_title'] = trans("crudbooster.label_button_profile");
		$data['row']        = CRUDBooster::first('cms_users',CRUDBooster::myId());
		$this->cbView('crudbooster::default.form',$data);
	}
	public function hook_before_edit(&$postdata,$id) {
		
		unset($postdata['password_confirmation']);
	}
	public function hook_before_add(&$postdata) {
		unset($postdata['password_confirmation']);
	}
}
