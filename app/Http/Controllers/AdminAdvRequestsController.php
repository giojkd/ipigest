<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App\AdvRequest;
use App\CmsUser;

class AdminAdvRequestsController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "name";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = (CRUDBooster::myId() == 4) ? true : false;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = true;
		$this->table = "adv_requests";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Nome campagna","name"=>"name"];
		$this->col[] = ["label"=>"Data ricezione","name"=>"created_at","callback"=>function($row){
			return date('d/m/Y H:i',strtotime($row->created_at));
		}];
		$this->col[] = ["label"=>"Data di pubblicazione","name"=>"publication_date","callback"=>function($row){
			return date('d/m/Y H:i',strtotime($row->publication_date));
		}];
		$this->col[] = ["label"=>"Canali social","name"=>"social_networks"];

		#$this->col[] = ["label"=>"Data inizio","name"=>"start_date"];
		#$this->col[] = ["label"=>"Data fine","name"=>"end_date"];
		$this->col[] = ["label"=>"Durata","name"=>"duration"];
		$this->col[] = ["label"=>"Spesa complessiva","name"=>"budget"];
		$this->col[] = ["label"=>"Agenzia","name"=>"agency_id","join"=>"agencies,name"];
		#$this->col[] = ["label"=>"Link annuncio su sito IPI","name"=>"link"];
		$this->col[] = ["label"=>"Status richiesta","name"=>"adv_request_status"];
		$this->col[] = ["label"=>"Step approvazione","name"=>"current_step","callback"=>function($row) {
			#$row->current_step
			ob_start();

			?>

			<table class="table">
				<thead>
					<tr>
						<th>IPI MKGT</th>
						<th>MGC</th>
						<th>AGENZIA<br>IPI MKGT</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td> <div style="height: 10px; width: 100%; background-color:<?=((int)$row->current_step > 1) ? 'green' : 'lightgray'?>"></div> </td>
						<td> <div style="height: 10px; width: 100%; background-color:<?=($row->current_step > 2) ? 'green' : 'lightgray'?>"></div> </td>
						<td> <div style="height: 10px; width: 100%; background-color:<?=($row->current_step > 3) ? 'green' : 'lightgray'?>"></div> </td>
					</tr>
				</tbody>
			</table>

			<?php

			$table = ob_get_clean();
			return $table;
		}];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Nome campagna','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'Nome campagna', 'required'=>true];
		$this->form[] = ['label'=>'Link annuncio su sito IPI','name'=>'link','type'=>'text','validation'=>'required','width'=>'col-sm-10','placeholder'=>'Please enter a valid URL', 'required'=>true];
		$this->form[] = ['label'=>'Punti forti dell\'immobile ','name'=>'pros','type'=>'textarea','validation'=>'string|min:5|max:5000','width'=>'col-sm-10','placeholder'=>'es. Bellissimo giardino e posizione centrale', 'required'=>true];
		$this->form[] = ['label'=>'Tipologia','name'=>'estate_type','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','placeholder'=>'Es. per famiglia, per professionisti, ecc...','required'=>true];
		$this->form[] = ['label'=>'Immobile in','name'=>'estate_contract_type','type'=>'select','validation'=>'min:1|max:255','width'=>'col-sm-10','dataenum'=>'Vendita;Affitto;Entrambi'];
		$this->form[] = ['label'=>'Target localizzazione','name'=>'target_geo','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','required'=>true];
		$this->form[] = ['label'=>'Target età','name'=>'target_age','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10','required'=>true];
		$this->form[] = ['label'=>'Altre specifiche del target','name'=>'target_gender','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Durata','name'=>'duration','type'=>'text','validation'=>'min:1|max:255','width'=>'col-sm-10'];
		#$this->form[] = ['label'=>'Data inizio','name'=>'start_date','type'=>'date','validation'=>'required|date','width'=>'col-sm-10', 'required'=>true];
		#$this->form[] = ['label'=>'Data fine','name'=>'end_date','type'=>'date','validation'=>'required|date','width'=>'col-sm-10', 'required'=>true];





		/*

		quali canali social (solo admin edit, utente normale solo lettura)
		link adv (solo admin edit, utente normale solo lettura)
		link landing (solo admin edit, utente normale solo lettura)
		note (solo admin edit, utente normale solo lettura)
		richieste agenzia al review (tutti)

		*/


		$this->form[] = ['label'=>'Foto (caricare solo file .zip)','name'=>'photos_zip','type'=>'filemanager','filemanager_type'=>'file','validation'=>'max:2048M', 'required' => true];

		$privilegeId = CRUDBooster::myPrivilegeId();

		if(CRUDBooster::isSuperadmin()){
			#$this->form[] = ['label'=>'Status','name'=>'adv_request_status','type'=>'select2','validation'=>'required','width'=>'col-sm-10',"dataenum"=>"Respinta causa foto;Respinta altro;Attesa validazione;Validata da agenzia;Richieste modifiche;Attesa validazione;Campagna attiva;Campagna terminata;Campagna sospesa;Campagna chiusa"];
			$this->form[] = ['label'=>'Status','name'=>'adv_request_status','type'=>'select2','validation'=>'required','width'=>'col-sm-10',"dataenum"=>"Campagna attiva;Campagna esaurita;Campagna sospesa;Campagna chiusa;In lavorazione;Ricevuto"];
			$this->form[] = ['label'=>'Spesa complessiva','name'=>'budget', 'placeholder' => '500,00', 'type'=>'money','validation'=>'integer|min:0','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Link ADV','name'=>'link_adv','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Link Landing','name'=>'link_landing','type'=>'text','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Note MGC Group','name'=>'adv_notes','type'=>'textarea','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Data di pubblicazione','name'=>'publication_date','type'=>'date','width'=>'col-sm-10'];

		}else{
			$this->form[] = ["label"=>"Status","name"=>"adv_request_status","type"=>"hidden","value"=>'Ricevuta'];

			$this->form[] = ['label'=>'Spesa complessiva','name'=>'budget','type'=>'hidden', "value"=> 500,'readonly' => true];
			$this->form[] = ['label'=>'Link ADV','name'=>'link_adv','type'=>'text','width'=>'col-sm-10','readonly' => true];
			$this->form[] = ['label'=>'Link Landing','name'=>'link_landing','type'=>'text','width'=>'col-sm-10','readonly' => true];
			$this->form[] = ['label'=>'Note MGC Group','name'=>'adv_notes','type'=>'textarea','width'=>'col-sm-10','readonly' => true];

		}
		$this->form[] = ['label'=>'Canali social','value'=>'FB,IG,LK','name'=>'social_networks','type'=>'text','width'=>'col-sm-10'];
		#$this->form[] = ['label'=>'Modifiche richieste dopo revisione','name'=>'review_requests','type'=>'textarea','width'=>'col-sm-10'];

		if($privilegeId == 2){
			$agencyIds = [];
			$userAgencies = DB::table('agency_cms_user')->where('cms_user_id',CRUDBooster::myId())->get();

			if(!is_null($userAgencies)){
				foreach($userAgencies as $agency){
					$agencyIds[] = $agency->agency_id;
				}
			};
			$agencyIds = implode(',',$agencyIds);
			$this->form[] = [
				'label'=>'Agency',
				'name'=>'agency_id',
				'type'=>'select2',
				'validation'=>'required|integer|min:0',
				'width'=>'col-sm-10',
				'datatable'=>'agencies,name',
				'datatable_where'=>'id IN('.$agencyIds.')'
			];
		}else{
			$this->form[] = [
				'label'=>'Agenzia',
				'name'=>'agency_id',
				'type'=>'select2',
				'validation'=>'required|integer|min:0',
				'width'=>'col-sm-10',
				'datatable'=>'agencies,name'
			];
		}

		#dd($this->form);

		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
		//$this->form[] = ["label"=>"Reference","name"=>"reference","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Link","name"=>"link","type"=>"text","required"=>TRUE,"validation"=>"required|url","placeholder"=>"Please enter a valid URL"];
		//$this->form[] = ["label"=>"Pros","name"=>"pros","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		//$this->form[] = ["label"=>"Start Date","name"=>"start_date","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
		//$this->form[] = ["label"=>"End Date","name"=>"end_date","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
		//$this->form[] = ["label"=>"Target Geo","name"=>"target_geo","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Target Age","name"=>"target_age","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Target Gender","name"=>"target_gender","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Budget","name"=>"budget","type"=>"money","required"=>TRUE,"validation"=>"required|integer|min:0"];
		//$this->form[] = ["label"=>"Estate Type","name"=>"estate_type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
		//$this->form[] = ["label"=>"Agency Id","name"=>"agency_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"agency,id"];
		//$this->form[] = ["label"=>"User Id","name"=>"user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"user,id"];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();



		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/
		$this->pre_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/
		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();



		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;



		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();


	}


	/*
	| ----------------------------------------------------------------------
	| Hook for button selected
	| ----------------------------------------------------------------------
	| @id_selected = the id selected
	| @button_name = the name of button
	|
	*/
	public function actionButtonSelected($id_selected,$button_name) {
		//Your code here

	}


	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate query of index result
	| ----------------------------------------------------------------------
	| @query = current sql query
	|
	*/
	public function hook_query_index(&$query) {
		//Your code here
		$privilegeId = CRUDBooster::myPrivilegeId();
		if($privilegeId == 2){
			$agencyIds = [];
			$userAgencies = DB::table('agency_cms_user')->where('cms_user_id',CRUDBooster::myId())->get();

			if(!is_null($userAgencies)){
				foreach($userAgencies as $agency){
					$agencyIds[] = $agency->agency_id;
				}
			};

			$query->whereIn('agency_id',$agencyIds);
		}
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate row of index table html
	| ----------------------------------------------------------------------
	|
	*/
	public function hook_row_index($column_index,&$column_value) {
		//Your code here
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before add data is execute
	| ----------------------------------------------------------------------
	| @arr
	|
	*/
	public function hook_before_add(&$postdata) {
		//Your code here


		$postdata['user_id'] = CRUDBOOSTER::myId();



		$agency = DB::table('agencies')->where('id',$postdata['agency_id'])->first();

		$postEdit = $postdata;


		$postEdit['agency_id'] = $agency->name;
		$postEdit['photos_zip'] = implode('/',[env('APP_URL'),$postEdit['photos_zip']]);
		$mailValue = 'New/Update Richiesta ADV '.$postdata['name'].'<br><br>';

		ob_start();

		?>
		<table><?php foreach($this->form as $field){?><tr><td><span><?=$field['label']?></span></td><td><?=$postEdit[$field['name']]?></td></tr><?php }?></table>
		<?php

		######################################################################
		##################### CUSTOMIZE THESE FIELDS #########################
		######################################################################

		$mailValue .= ob_get_clean(); #<---- customize

		$subject = 'New/Update Richiesta ADV '.$postdata['name'];
		$mailTo = [['email'=>'l.gannio@ipi-spa.com'],['email'=>'a.bonichi@ipi-spa.com'],['email'=>'l.franzero@ipi-spa.com'],['email'=>'massimiliano@mgc-group.it']];
		#$mailTo = [['email'=>'giojkd@gmail.com']];
		$mailFrom = ['email'=>'noreply@ipi-spa.com','name'=>'IPIGEST'];

		#######################################################################
		#######################################################################
		#######################################################################



		$endpoint = 'https://api.sendgrid.com/v3/mail/send';
		#$post_json = '{"personalizations": [{"to": '.json_encode($mailTo).'}],"from": {"email": "massimiliano@mgc-group.it","name":"Mgc Group"},"subject": "Nuova richiesta ADV","content": [{"type": "text/html", "value": "'.$mailValue.'"}]}';
		$personalizations = array();
		$personalizations = [
			'to' => $mailTo,
		];
		$post = [
			'personalizations' => [$personalizations],
			'from'=>$mailFrom,
			'subject' => $subject,
			'content' => [
				[
					'type' => 'text/html',
					'value' => $mailValue
				]
			]
		];
		$post_json = json_encode($post);
		$ch = @curl_init();
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_json);  //Post Fields
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
			'Content-Type: application/json',
			'Authorization: Bearer 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'
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);

		$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$curl_errors = curl_error($ch);
		curl_close ($ch);


	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after add public static function called
	| ----------------------------------------------------------------------
	| @id = last insert id
	|
	*/
	public function hook_after_add($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before update data is execute
	| ----------------------------------------------------------------------
	| @postdata = input post data
	| @id       = current id
	|
	*/
	public function hook_before_edit(&$postdata,$id) {
		//Your code here

		#$this->hook_before_add($postdata);

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after edit public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_edit($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command before delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_before_delete($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_delete($id) {
		//Your code here

	}

	public function getDetail($id) {
		//Create an Auth
		if(!CRUDBooster::isRead() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}
		$this->cbInit();

		$data = [];
		$data['page_title'] = 'Detail Data';
		$request = AdvRequest::with(['user','feedbacks','feedbacks.user','feedbacks.user.groups','agency'])->find($id);



		$currentStep = $request->step();

		$request->current_step = $currentStep;
		$request->save();


		$request = $request->toArray();

		$request['created_at'] = date('d/m/Y H:i:s',strtotime($request['created_at']));
		$request['updated_at'] = (strtotime($request['updated_at']) > 0) ? date('d/m/Y H:i:s',strtotime($request['updated_at'])) : 'Mai';

		$data['request'] = $request;
		$data['agency'] = $data['request']['agency'];
		$data['user'] = $data['request']['user'];

		$feedbacks = [];
		$feedbacks_ = $data['request']['feedbacks'];

		foreach($feedbacks_ as $item){
			if($item['step']<=$currentStep)
			$feedbacks[$item['step']][] = $item;
		}

		for($i = 1 ; $i <= $currentStep; $i++){
			$feedbacks[$i] = (isset($feedbacks[$i])) ? $feedbacks[$i] : [];
		}

		$data['currentStep'] = $currentStep;
		$data['feedbacksByStep'] = $feedbacks;
		unset($data['request']['user']);
		unset($data['request']['agency']);
		unset($data['request']['feedbacks']);

		$otherColumns = [
			[
				'name' => 'created_at',
				'label' => 'Creato il'
			],
			[
				'name' => 'updated_at',
				'label' => 'Modificato il'
			]
		];

		$form = $this->form;

		$excluded_fields = ['id','reference','start_date','end_date','user_id','review_requests','current_step','agency_id'];

		$data['excluded_fields'] = $excluded_fields;

		$data['form'] = array_merge($form,$otherColumns);

		$currentuser = CmsUser::find(CRUDBooster::myId());



		$buttonsByStep = [];

		if($currentuser->isCustomer()){
			$buttonsByStep = [
				1 => [
					['label' => 'Accetta', 'value' => 'accepted','type'=>'success'],
					['label' => 'Rifiuta', 'value' => 'rejected','type'=>'danger']
				],
				2 => [],
				3 => [
					['label' => 'Accetta', 'value' => 'accepted','type'=>'success'],
					['label' => 'Rifiuta', 'value' => 'rejected','type'=>'danger']
				]
			];
		}

		if($currentuser->isWebAgency()){
			$buttonsByStep = [
				1 => [],
				2 =>  [
					['label' => 'Accetta', 'value' => 'accepted','type'=>'success'],
					['label' => 'Rifiuta', 'value' => 'rejected','type'=>'danger']
				],
				3 => []
			];
		}

		if($currentuser->isAgency()){
			$buttonsByStep = [
				1 => [],
				2 =>  [],
				3 => [
					['label' => 'Accetta', 'value' => 'accepted','type'=>'success'],
					['label' => 'Rifiuta', 'value' => 'rejected','type'=>'danger']
				]
			];
		}

		$data['buttonsByStep'] = $buttonsByStep;


		$data['feedback_labels'] = ['accepted' =>'success','rejected'=>'danger' ];



		//Please use cbView method instead view method from laravel

		$data['step_names'] = [1=>'Approvazione IPI Marketing',2=>'Approvazione MGC Group',3=>'Approvazione ADV Agenzia | IPI Marketing'];
		$data['step_colors'] = [1=>'#3c8cbc',2=>'black',3=>'#00a65a'];

		$this->cbView('custom_adv_request_detail_view',$data);
	}

	//By the way, you can still create your own method in here... :)

	public function requestApproval($id){
		$request = DB::table('adv_requests')->where('id',$id)->first();
		$request = (array)$request;

		$this->cbInit();
		$this->hook_before_add($request);
		CRUDBooster::redirect(CRUDBooster::adminPath()."/adv_requests/detail/".$id,"Richiesta di approvazione inviata","success");
	}

}
