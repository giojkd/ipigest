<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use \App\AdvRequestFeedback;
use \App\Http\Controllers\AdvRequestController;
use \App\AdvRequestFeedbackSummary;
use \App\UserGroup;
class AdminAdvRequestFeedbacksController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "adv_request_feedbacks";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Utente","name"=>"cms_user_id","join"=>"cms_users,name"];
		$this->col[] = ["label"=>"Feedback Type","name"=>"feedback_type"];
		$this->col[] = ["label"=>"Feedback Notes","name"=>"feedback_notes"];
		$this->col[] = ["label"=>"Adv Request Id","name"=>"adv_request_id","join"=>"adv_requests,name"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Utente','name'=>'cms_user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
		$this->form[] = ['label'=>'Feedback Type','name'=>'feedback_type','type'=>'text','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Step','name'=>'step','type'=>'text','validation'=>'required','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Feedback Notes','name'=>'feedback_notes','type'=>'textarea','validation'=>'','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Adv Request Id','name'=>'adv_request_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'adv_requests,name'];
		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ["label"=>"Cms User Id","name"=>"cms_user_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_user,id"];
		//$this->form[] = ["label"=>"Feedback Type","name"=>"feedback_type","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		//$this->form[] = ["label"=>"Feedback Notes","name"=>"feedback_notes","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
		//$this->form[] = ["label"=>"Adv Request Id","name"=>"adv_request_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"adv_request,id"];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();



		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/
		$this->pre_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/
		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();



		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;



		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();


	}


	/*
	| ----------------------------------------------------------------------
	| Hook for button selected
	| ----------------------------------------------------------------------
	| @id_selected = the id selected
	| @button_name = the name of button
	|
	*/
	public function actionButtonSelected($id_selected,$button_name) {
		//Your code here

	}


	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate query of index result
	| ----------------------------------------------------------------------
	| @query = current sql query
	|
	*/
	public function hook_query_index(&$query) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate row of index table html
	| ----------------------------------------------------------------------
	|
	*/
	public function hook_row_index($column_index,&$column_value) {
		//Your code here
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before add data is execute
	| ----------------------------------------------------------------------
	| @arr
	|
	*/
	public function hook_before_add(&$postdata) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after add public static function called
	| ----------------------------------------------------------------------
	| @id = last insert id
	|
	*/
	public function hook_after_add($id) {
		//Your code here



		$feedback = AdvRequestFeedback::with(['user','adv_request','adv_request.user'])->find($id);




		/*Save feedback summary*/

		if($feedback->user->isWebAgency()){
			$user_type = 'web_agency';
		}
		if($feedback->user->isCustomer()){
			$user_type = 'customer';
		}
		if($feedback->user->isAgency()){
			$user_type = 'agency';
		}
		$summary = AdvRequestFeedbackSummary::firstOrCreate([
			'adv_request_id' => $feedback->adv_request->id,
			'step' => $feedback->step,
			'user_type' => $user_type
		]);
		$summary -> feedback = $feedback->feedback_type;
		$summary->save();

		/*Save feedback summary*/

		$requestUserEmail = $feedback->adv_request->user->email;



		$feedback = $feedback->toArray();
		$request = $feedback['adv_request'];

		$requestController = new AdminAdvRequestsController();
		$requestController->cbInit();

		$otherColumns = [
			[
				'name' => 'created_at',
				'label' => 'Creato il'
			],
			[
				'name' => 'updated_at',
				'label' => 'Modificato il'
			]
		];

		$form = array_merge($requestController->form,$otherColumns);


		$excluded_fields = ['id','reference','start_date','end_date','user_id','review_requests','current_step','agency_id','user'];
		foreach($excluded_fields as $excludedField){
			unset($request[$excludedField]);
		}


				$request['created_at'] = date('d/m/Y H:i:s',strtotime($request['created_at']));
				$request['updated_at'] = (strtotime($request['updated_at']) > 0) ? date('d/m/Y H:i:s',strtotime($request['updated_at'])) : 'Mai';

		ob_start();
		?><table><?php foreach ($request as $key => $value):$label = '';?><tr><td><b><?php foreach($form as $formVal){if($formVal['name']==$key) $label = $formVal['label'];} if($label !='' ) echo $label; else echo $key?></b></td><td><?=($value!='') ? $value : 'NON ANCORA DISPONIBILE'?></td></tr><?php endforeach; ?></table><?php
		$request_summary = ob_get_clean();

#dd($feedback);
		ob_start();
		?><table><tr><td>Da</td><td><?=$feedback['user']['name']?></td></tr><tr><td>Commento</td><td><?=$feedback['feedback_notes']?></td></tr><tr><td>Il</td><td><?=date('d/m/Y H:i',strtotime($feedback['created_at']))?></td></tr><tr><td>Stato</td><td><span style='color:#fff; display: inline-block; padding: 5px; background-color:<?=($feedback['feedback_type'] == 'accepted') ? 'green' : 'red'?>'><?=($feedback['feedback_type'] == 'accepted') ? 'Accettata' : 'Rifiutata'?></span></td></tr></table><?php
		$feedback_summary = ob_get_clean();
		#$feedback_summary = htmlentities($feedback_summary);
		$adv_request_link ="http://ipigest.mgc-group.it/adv_requests/detail/".$feedback['adv_request_id'];
		$data = [
			'request_summary' => $request_summary,
			'feedback_summary' => $feedback_summary,
			'adv_request_link' => $adv_request_link,
			'request_name' => $request['name']
		];





		$recipients = [];

		$recipientGroups = UserGroup::with('users')->find([1,2]);




		foreach($recipientGroups as $group){
			foreach($group->users as $user){
					$recipients[] = $user->email;
			}
		}

		$recipients[] = $requestUserEmail;

		$deliveryFeeback = [];

		$emailTemplate = DB::table('cms_email_templates')->where('slug','new_request_feedback_notification')->first();

		$mailContent = $emailTemplate->content;
		$mailSubject = $emailTemplate->subject;

		foreach($data as $index => $value){
			$mailContent = str_replace('['.$index.']', $value, $mailContent);
		}

		foreach($data as $index => $value){
			$mailSubject = str_replace('['.$index.']', $value, $mailSubject);
		}



		################
		################
		################
		################


		foreach($recipients as $recipient){


		$mailValue = $mailContent;

		$endpoint = 'https://api.sendgrid.com/v3/mail/send';
		$post_json = '{"personalizations": [{"to": [{"email": "'.$recipient.'"}]}],"from": {"email": "noreply@immobiliareipi.com"},"subject": "'.$mailSubject.'","content": [{"type": "text/html", "value": "'.$mailValue.'"}]}';
		#echo $post_json;
		$ch = @curl_init();
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_json);  //Post Fields
		curl_setopt($ch, CURLOPT_URL,$endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$headers = [
		  'Content-Type: application/json',
		  'Authorization: Bearer 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'
		];

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);

		$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$curl_errors = curl_error($ch);
		curl_close ($ch);

	}

#	exit;

		################
		################
		################
		################

/*

		foreach($recipients as $recipient){
				$deliveryFeeback[] = CRUDBooster::sendEmail(['to'=>$recipient,'data'=>$data,'template'=>'new_request_feedback_notification']);
		}

*/

		CRUDBooster::redirect(CRUDBooster::adminPath()."/adv_requests/detail/".$feedback['adv_request_id'],"Feedback aggiunto correttamente","success");
		exit;
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before update data is execute
	| ----------------------------------------------------------------------
	| @postdata = input post data
	| @id       = current id
	|
	*/
	public function hook_before_edit(&$postdata,$id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after edit public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_edit($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command before delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_before_delete($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_delete($id) {
		//Your code here

	}



	//By the way, you can still create your own method in here... :)


}
