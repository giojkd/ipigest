<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\LeadGroup;
use App\WebhookLog;
use App\Lead;

use CRUDBooster;

class LeadController extends Controller
{


  public function sendTestEmail(Request $request){

      $leadGroup = LeadGroup::find(5);
      $lead = Lead::find(311);

      if(!is_null($leadGroup->send_email_to)){
        $recipients = explode(',',$leadGroup->send_email_to);
        if(count($recipients)>0){
          foreach($recipients as $recipient){
            $data = [
              'name' => $lead['nome_e_cognome'],
              'email' => $lead['e-mail'],
              'telephone' => $lead['numero_di_telefono'],
              'company' => '',
              'campaign' => $leadGroup->name
            ];
            if($recipient!= ''){
              $sendStatus = CRUDBooster::sendEmail(['to'=>trim($recipient),'data'=>$data,'template'=>'new_lead_facebook_ads']);
              echo $sendStatus;
            }
          }
        }
      }

  }

  public function FacebookWebhook(Request $request){
    $data = $request->all();
    #$data = file_get_contents("php://input");
    DB::table('webhook_logs')->insert(['content' => json_encode($data)]);
  }

  public function ParseFacebookLeads(Request $request){
    $hooks = WebhookLog::where('is_parsed',0)->get();

    if(!is_null($hooks)){
      foreach($hooks as $hook){
        $lead = json_decode($hook->content,1);
        if($lead['campaign_id'] != ''){
          $leadGroup = LeadGroup::where('facebook_campaign_id',$lead['campaign_id'])->first();
          if(!is_null($leadGroup)){

            $newLead  = new Lead([
              'name' => $lead['nome_e_cognome'],
              'email' => $lead['e-mail'],
              'telephone' => $lead['numero_di_telefono'],
              'leadgroup_id' => $leadGroup['id'],
              'lead_source' => 'Facebook Ads'
            ]);
            $newLead->save();

            if(!is_null($leadGroup->send_email_to)){
              $recipients = explode(',',$leadGroup->send_email_to);
              if(count($recipients)>0){
                foreach($recipients as $recipient){
                  $data = [
                    'name' => $lead['nome_e_cognome'],
                    'email' => $lead['e-mail'],
                    'telephone' => $lead['numero_di_telefono'],
                    'company' => '',
                    'campaign' => $leadGroup->name
                  ];
                  if($recipient!= ''){
                    CRUDBooster::sendEmail(['to'=>trim($recipient),'data'=>$data,'template'=>'new_lead_facebook_ads']);
                  }
                }
              }
            }

            $hook->is_parsed = 1;
            $hook->save();
          }else{
            $hook->is_parsed = -1;
            $hook->save();
          }
        }else {
          $hook->is_parsed = -1;
          $hook->save();
        }

      }
    }
  }
}
