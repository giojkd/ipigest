<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\AdvRequestFeedback;
use \App\AdvRequestFeedbackSummary;

class AdvRequest extends Model
{

  public function agency(){
      return $this->belongsTo('\App\Agency');
  }

  public function user(){
    return $this->belongsTo('\App\CmsUser');
  }
  public function feedbacks(){
    return $this->hasMany('\App\AdvRequestFeedback');
  }

  public function step(){
    $feedback = AdvRequestFeedback::with('user')->where('adv_request_id',$this->id)->where('step',1)->orderBy('id','desc')->first();
    if($feedback->feedback_type == 'accepted' && $feedback->user->isCustomer()){
      $step = 2;
    }else{
      $step = 1;
    }

    if($step == 2){
      $feedback = AdvRequestFeedback::with('user')->where('adv_request_id',$this->id)->where('step',2)->orderBy('id','desc')->first();
      if($feedback->feedback_type == 'accepted' && $feedback->user->isWebAgency()){
        $step = 3;
      }else{
        $step = 2;
      }
    }

    if($step == 3){
      $agencyFeedback = AdvRequestFeedbackSummary::where('adv_request_id',$this->id)->where('step',3)->where('user_type','agency')->first();

      $customerFeedback = AdvRequestFeedbackSummary::where('adv_request_id',$this->id)->where('step',3)->where('user_type','customer')->first();

      if($customerFeedback->feedback == 'accepted' && $agencyFeedback->feedback == 'accepted') {
          $step = 4;
      }else {
        $step = 3;
      }

    }

    return $step;

  }

  protected $table = 'adv_requests';
}
