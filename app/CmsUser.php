<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsUser extends Model
{

  protected $table = 'cms_users';

  public function groups(){
    return $this->belongsToMany('\App\UserGroup');
  }

  public function isWebAgency(){
    $webAgencyId = 1;
    $groups = $this->groups()->find($webAgencyId);
    if(is_null($groups)){
      return false;
    }
    return true;
  }

  public function isCustomer(){
    $customerId = 2;
    $groups = $this->groups()->find($customerId);
    if(is_null($groups)){
      return false;
    }
    return true;
  }

  public function isAgency(){
    $agencyId = 3;
    $groups = $this->groups()->find($agencyId);
    if(is_null($groups)){
      return false;
    }
    return true;
  }

}
