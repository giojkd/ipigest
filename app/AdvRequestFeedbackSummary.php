<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvRequestFeedbackSummary extends Model
{
  protected $table = 'adv_request_feedbacks_summaries';
  protected $guarded = [];
}
